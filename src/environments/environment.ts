// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCm0yQ8nsbIsjbMpnme1bLM-SC_97OSq9E',
    authDomain: 'mappe-della-resistenza.firebaseapp.com',
    databaseURL: 'https://mappe-della-resistenza.firebaseio.com',
    projectId: 'mappe-della-resistenza',
    storageBucket: 'mappe-della-resistenza.appspot.com',
    messagingSenderId: '89713376711'
  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoibG9wZXptYXBzIiwiYSI6ImNqNDB3YnJtcDAxNDMzMnMyNWZmMGZ3a3IifQ.2VbIS4DW-DHklfXmg1vlKw',
    styleUrl: 'mapbox://styles/lopezmaps/cjsn1bgsb00h21fnqallsnlbf'
  },
  cloudinary: {
    cloud_name: 'gianluca-macaluso',
    upload_preset: 'mappe-resistenza'
  }
};
