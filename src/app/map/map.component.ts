import { Component, OnInit, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { first } from 'rxjs/operators';

import * as mapboxgl from 'mapbox-gl';
import { ResizedEvent } from 'angular-resize-event';

import { environment } from '../../environments/environment';

import { SymbolsService } from '../services/symbols.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() styleUrl = environment.mapbox.styleUrl;
  @Input() containerH = '100%';
  @Input() markerW = '15px';
  @Input() markerH = '15px';
  @Input() subMarkerW = '25px';
  @Input() subMarkerH = '25px';
  @Input() markerColor = '#56267D';
  @Input() markerTextColor = '#ffffff';
  @Input() markerFontSize = '10px';
  @Input() subMarkerFontSize = '12px';
  @Input() strokeWidth = 20;
  @Input() fillColor = '#56267D';
  @Output() mapLoaded = new EventEmitter<boolean>();
  @Output() mapClicked = new EventEmitter<any>();
  @Output() markerClicked = new EventEmitter<string>();

  private mapInstance: mapboxgl.Map;
  private defaultLocation: number[] = [11.257605195576048, 44.20785479455327];
  private defaultZoom = 6;
  private cityLevelZoom = 10.5;

  private isFlyingToMarkergroup = false;
  private markerGroupDestCityID: string = null;
  private subMarkerClicked: HTMLElement = null;

  public markers: mapboxgl.Marker[];
  public currentMarkerIndex = 0;
  public layerSwitches: any;
  public categorySwitches: any;

  private clusterData: GeoJSON.FeatureCollection = {
    type: 'FeatureCollection',
    features: []
  };
  private cityLabelsCollection: GeoJSON.FeatureCollection = {
    type: 'FeatureCollection',
    features: []
  };

  private firstLineLayerID: string;

  constructor(private zone: NgZone, private symbolsService: SymbolsService) {
    this.zone.onStable.pipe(first()).subscribe(() => {
      this.assign(mapboxgl, 'accessToken', environment.mapbox.accessToken);
      this.createMap();
    });
  }

  ngOnInit() {
  }

  private createMap() {
    this.zone.runOutsideAngular(() => {
      this.mapInstance = new mapboxgl.Map({
        container: 'map',
        style: this.styleUrl,
        zoom: this.defaultZoom - 0.5,
        center: this.defaultLocation as mapboxgl.LngLatLike
      });

      this.mapInstance.getCanvasContainer().style.cursor = 'default';
      this.mapInstance.getContainer().style.height = this.containerH;
      this.mapInstance.resize();
      this.mapInstance.setMaxBounds( this.mapInstance.getBounds() );
      this.mapInstance.setZoom( this.defaultZoom );
      this.mapInstance.setMinZoom( this.defaultZoom );
      this.mapInstance.setMaxZoom( this.cityLevelZoom );

      this.mapInstance.on('load', () => { this.mapLoaded.emit( true ); });

      this.mapInstance.on('moveend', (e: any) => {
        if (this.isFlyingToMarkergroup && this.subMarkerClicked) {
          this.isFlyingToMarkergroup = false;

          const parent = this.subMarkerClicked.parentElement;
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0;  i < parent.childNodes.length; i++) {
            const childNode = (parent.childNodes[i] as HTMLElement);
            if ( childNode.classList.contains('sub-marker') ) {
              const toSub = setTimeout(() => {
                childNode.classList.remove('hidden');
                clearTimeout(toSub);
              }, i * 100);
            } else {
              childNode.classList.add('shrink');
            }
          }

          this.subMarkerClicked = null;

          this.markerClicked.emit( this.markerGroupDestCityID );
        }
      });

      this.mapInstance.on('zoomend', () => {
        if ( this.mapInstance.getZoom() < 10 ) {
          document.querySelectorAll('.sub-marker').forEach((element: HTMLElement) => {
            element.classList.add('hidden');
          });

          document.querySelectorAll('.marker-group').forEach((element: HTMLElement) => {
            element.classList.remove('shrink');
          });
        }
      });

      this.mapInstance.on('click', 'clusters', (e: any) => {
        if ( this.mapInstance.getLayer('clusters') ) {
          const clusterFeatures = this.mapInstance.queryRenderedFeatures(e.point, { layers: ['clusters'] });
          if (clusterFeatures && clusterFeatures.length > 0) {
            const srcID = clusterFeatures[0].source;
            const clusterId = clusterFeatures[0].properties.cluster_id;

            if ( clusterFeatures[0].layer.id === 'clusters' ) {
              (this.mapInstance.getSource(srcID) as mapboxgl.GeoJSONSource)
                .getClusterExpansionZoom(clusterId, (err: any, clusterZoom: any) => {
                  if (err) { return; }

                  // tslint:disable-next-line: no-string-literal
                  const destLocation = (clusterFeatures[0].geometry['coordinates'] as mapboxgl.LngLatLike);

                  this.mapInstance.easeTo({
                    center: destLocation,
                    zoom: clusterZoom
                  });
                });
            }
          }
        }
      });

      this.mapInstance.on('click', 'unclustered-point', (e: any) => {
        if ( this.mapInstance.getLayer('unclustered-point') ) {
          const clusterFeatures = this.mapInstance.queryRenderedFeatures(e.point, { layers: ['unclustered-point'] });
          if (clusterFeatures && clusterFeatures.length > 0) {
            this.zone.run(() => {
              this.markerClicked.emit(clusterFeatures[0].properties.cityID);
            });

            // const srcID = clusterFeatures[0].source;
            // const clusterId = clusterFeatures[0].properties.cluster_id;

            // for (const cat in clusterFeatures) {
            //   if (cat && clusterFeatures[cat].properties.quantity > 0) {
            //     console.log(
            //       clusterFeatures[cat].properties.cityName,
            //       clusterFeatures[cat].properties.catName,
            //       clusterFeatures[cat].properties.quantity
            //     );
            //   }
            // }
          }
        }
      });

      this.mapInstance.on('mouseenter', 'clusters', () => { this.mapInstance.getCanvas().style.cursor = 'pointer'; });
      this.mapInstance.on('mouseleave', 'clusters', () => { this.mapInstance.getCanvas().style.cursor = ''; });
      this.mapInstance.on('mouseenter', 'unclustered-point', () => { this.mapInstance.getCanvas().style.cursor = 'pointer'; });
      this.mapInstance.on('mouseleave', 'unclustered-point', () => { this.mapInstance.getCanvas().style.cursor = ''; });
    });
  }

  private addCityMarker( cityData: any, markerData: any, index: number ) {
    // console.log(cityData.name);
    const elContainer = document.createElement('div');
    elContainer.classList.add('marker-container');
    elContainer.classList.add(markerData.id);
    if (!this.categorySwitches[markerData.id].visible) { elContainer.classList.add('hidden'); }
    elContainer.setAttribute('style', `
      z-index: ${this.getMarkerParams(markerData.slug).index};
    `);

    const el = document.createElement('div');
    el.id = 'marker-' + markerData.id;
    el.classList.add('marker', markerData.slug, 'in');
    if (
      markerData.slug === 'tribunale-militare-straordinario-di-guerra'
      || markerData.slug === 'tribunale-speciale-per-la-difesa-dello-stato'
      || markerData.slug === 'tribunale-speciale-straordinario' 
      || markerData.slug === 'sezione-tribunale-speciale-per-la-difesa-dello-stato'
    ) {
      el.innerHTML = this.symbolsService.getSymbol('star', markerData.color);

      let margins: any;
      switch (this.getMarkerParams(markerData.slug).position) {
        case 'top-right':
          margins = {top: '-30px', left: '30px'};
          break;
        case 'bottom-right':
          margins = {top: '30px', left: '30px'};
          break;
        case 'bottom-left':
          margins = {top: '30px', left: '-30px'};
          break;
        case 'top-left':
          margins = {top: '-30px', left: '-30px'};
          break;
      }

      el.setAttribute('style', `
        width: 20px;
        height: 20px;
        margin-left: ${margins.left};
        margin-top: ${margins.top};
      `);
    } else {
      el.setAttribute('style', `
        width: ${this.getMarkerParams(markerData.slug).radius}px;
        height: ${this.getMarkerParams(markerData.slug).radius}px;
        background-color: ${markerData.color};
        color: ${this.markerTextColor};
        font-size: ${this.markerFontSize};
        border-radius: 50%;
      `);
    }

    elContainer.appendChild(el);
    elContainer.addEventListener('click', (e: MouseEvent) => {
      this.markerClicked.emit(cityData.id);
    });

    const m = new mapboxgl.Marker( elContainer )
      .setLngLat( [cityData.location.longitude, cityData.location.latitude] as mapboxgl.LngLatLike )
      .addTo( this.mapInstance );

    this.markers ? this.markers.push( m ) : this.markers = [ m ];

    const to = setTimeout(() => {
      el.classList.remove('in');
      clearTimeout(to);
    }, index * 10);
  }

  private addMarkerGroup( groupData: any, index: number ) {
    const elContainer = document.createElement('div');
    elContainer.classList.add('marker-container', 'marker-group-container');

    let numItems = 0;
    let numSlices = 0;
    let markerSize: number;

    for (const cat of groupData.data ) {
      if (this.categorySwitches[cat.category.id].visible) { 
        numItems += cat.quantity;
        numSlices++;
      }
    }

    if (numItems === 0) { return; }

    numItems < 4 ? markerSize = 15 : markerSize = numItems * 5;

    const radius = markerSize / 2;
    const start = 0;
    const slice = 360 / numSlices;

    const el = document.createElement('div');
    el.classList.add('marker', 'marker-group', 'in');
    el.innerHTML = numItems.toString();
    el.setAttribute('style', `
      width: ${markerSize}px;
      height: ${markerSize}px;
      background-color: rgba(1, 187, 214, .75);
      color: ${this.markerTextColor};
      font-size: ${this.markerFontSize};
      border-radius: 50%;
      border: 1px solid rgba(255, 255, 255, .25);
    `);

    elContainer.appendChild(el);

    let count = 0;
    for (const cat of groupData.data ) {
      if (this.categorySwitches[cat.category.id].visible) {
        const rotate = slice * count + start;

        const subEl = document.createElement('div');
        subEl.classList.add('marker', 'sub-marker');
        subEl.classList.add(cat.category.id);
        subEl.setAttribute('style', `
          width: ${this.subMarkerW};
          height: ${this.subMarkerH};
          background-color: ${cat.category.color};
          color: ${this.markerTextColor};
          font-size: ${this.subMarkerFontSize};
          border-radius: 50%;
          border: 1px solid rgba(255, 255, 255, .25);
        `);

        subEl.innerHTML = cat.quantity.toString();
        subEl.style.transform = `scale(1) rotate(${rotate}deg) translate(25px) rotate(${rotate * -1}deg)`;
        subEl.classList.add('hidden');

        elContainer.appendChild(subEl);
        count++;
      }
    }

    elContainer.addEventListener('click', (e: MouseEvent) => {
      document.querySelectorAll('.sub-marker').forEach((element: HTMLElement) => {
        element.classList.add('hidden');
      });

      document.querySelectorAll('.marker-group').forEach((element: HTMLElement) => {
        element.classList.remove('shrink');
      });

      this.isFlyingToMarkergroup = true;
      this.markerGroupDestCityID = groupData.groupCity.id;
      this.subMarkerClicked = e.target as HTMLElement;

      this.mapInstance.easeTo({
        center: [groupData.groupCity.location.longitude, groupData.groupCity.location.latitude],
        zoom: this.cityLevelZoom
      });
    });

    const m = new mapboxgl.Marker( elContainer )
      .setLngLat( [groupData.groupCity.location.longitude, groupData.groupCity.location.latitude] as mapboxgl.LngLatLike )
      .addTo( this.mapInstance );

    this.markers ? this.markers.push( m ) : this.markers = [ m ];

    const to = setTimeout(() => {
      el.classList.remove('in');
      clearTimeout(to);
    }, index * 10);
  }

  private renderLineLayer(layerID: string, props: any, lines: any) {
    const lineCollection = {
      type: 'FeatureCollection',
      features: []
    };

    for (const line in lines) {
      if (line) {
        const lineFeature = {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'LineString',
            coordinates: []
          }
        };

        for (const point in lines[line]) {
          if (point) {
            lineFeature.geometry.coordinates.push( lines[line][point] );
          }
        }

        lineCollection.features.push( (lineFeature as GeoJSON.Feature) );
      }
    }

    if ( this.mapInstance.getLayer(layerID) ) {
      if (props.strokeWidth) {
        this.mapInstance.setPaintProperty(layerID, 'line-width', props.strokeWidth);
      } else {
        this.mapInstance.setPaintProperty(layerID, 'line-width', this.strokeWidth);
      }

      if (props.hasDashedStroke) {
        this.mapInstance.setPaintProperty(layerID, 'line-dasharray', [props.dashPattern.strokeWidth, props.dashPattern.spacing]);
      } else {
        this.mapInstance.setPaintProperty(layerID, 'line-dasharray', null);
      }

      (this.mapInstance.getSource(layerID) as mapboxgl.GeoJSONSource).setData( (lineCollection as GeoJSON.FeatureCollection) );
    } else {
      const paintOptions = {
        'line-color': props.strokeColor ? props.strokeColor : this.markerColor,
        'line-width': props.strokeWidth ? props.strokeWidth : this.strokeWidth
      };
      if (props.hasDashedStroke) { paintOptions['line-dasharray'] = [props.dashPattern.strokeWidth, props.dashPattern.spacing]; }

      this.mapInstance.addLayer({
        id: layerID,
        type: 'line',
        source: {
          type: 'geojson',
          data: (lineCollection as GeoJSON.FeatureCollection)
        },
        layout: {
          // tslint:disable-next-line: object-literal-key-quotes
          'visibility': 'visible',
          'line-join': 'round',
          'line-cap': 'round'
        },
        paint: paintOptions
      });
    }
  }

  private renderAreaLayer(layerID: string, props: any, areas: any) {
    const areaCollection = {
      type: 'FeatureCollection',
      features: []
    };

    for (const area in areas) {
      if (area) {
        const areaFeature = {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [[]]
          }
        };

        for (const point in areas[area]) {
          if (point) {
            areaFeature.geometry.coordinates[0].push( areas[area][point] );
          }
        }

        areaCollection.features.push( (areaFeature as GeoJSON.Feature) );
      }
    }

    if ( this.mapInstance.getLayer(layerID) ) {
      if (props.fillColor) {
        this.mapInstance.setPaintProperty(layerID, 'fill-color', props.fillColor);
      } else {
        this.mapInstance.setPaintProperty(layerID, 'fill-color', 'rgba(0,0,0,0)');
      }

      let isVisible = '';
      this.layerSwitches[layerID].visible ? isVisible = 'visible' : isVisible = 'none';
      this.mapInstance.setLayoutProperty(layerID, 'visibility', isVisible);

      (this.mapInstance.getSource(layerID) as mapboxgl.GeoJSONSource).setData( (areaCollection as GeoJSON.FeatureCollection) );
    } else {
      const paintOptions = {
        'fill-color': props.fillColor ? props.fillColor : this.fillColor,
      };

      if (props.hasDashedStroke) { paintOptions['line-dasharray'] = [props.dashPattern.strokeWidth, props.dashPattern.spacing]; }

      if (this.firstLineLayerID) {
        this.mapInstance.addLayer({
          id: layerID,
          type: 'fill',
          source: {
            type: 'geojson',
            data: (areaCollection as GeoJSON.FeatureCollection)
          },
          layout: {
            // tslint:disable-next-line: object-literal-key-quotes
            'visibility': this.layerSwitches[layerID].visible ? 'visible' : 'none'
          },
          paint: paintOptions
        }, this.firstLineLayerID);
      } else {
        this.mapInstance.addLayer({
          id: layerID,
          type: 'fill',
          source: {
            type: 'geojson',
            data: (areaCollection as GeoJSON.FeatureCollection)
          },
          layout: {
            // tslint:disable-next-line: object-literal-key-quotes
            'visibility': this.layerSwitches[layerID].visible ? 'visible' : 'none'
          },
          paint: paintOptions
        });
      }
    }
  }

  private renderClusters() {
    this.mapInstance.addSource('clusterSource', {
      type: 'geojson',
      data: this.clusterData,
      cluster: true,
      clusterMaxZoom: 14, // Max zoom to cluster points on
      clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
    });

    this.mapInstance.addLayer({
      id: 'clusters',
      type: 'circle',
      source: 'clusterSource',
      filter: ['has', 'point_count'],
      paint: {
        // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
        // with three steps to implement three types of circles:
        //   * Blue, 20px circles when point count is less than 100
        //   * Yellow, 30px circles when point count is between 100 and 750
        //   * Pink, 40px circles when point count is greater than or equal to 750
        'circle-color': [
          'step',
          ['get', 'point_count'],
          'rgba(1, 187, 214, .5)', 10, 'rgba(1, 187, 214, .7)', 20, 'rgba(1, 187, 214, .9)'
        ],
        'circle-radius': [
          'step',
          ['get', 'point_count'],
          20, 10, 30, 20, 40
        ]
      }
    });

    this.mapInstance.addLayer({
      id: 'cluster-count',
      type: 'symbol',
      source: 'clusterSource',
      filter: ['has', 'point_count'],
      layout: {
        'text-field': '{point_count_abbreviated}',
        'text-size': 12
      }
    });

    this.mapInstance.addLayer({
      id: 'unclustered-point',
      type: 'circle',
      source: 'clusterSource',
      filter: ['!', ['has', 'point_count']],
      paint: {
        'circle-color': 'rgba(1, 187, 214, .3)',
        'circle-radius': 3,
        'circle-stroke-width': 7,
        'circle-stroke-color': 'rgba(1, 187, 214, 1)'
      }
    });
  }

  private addCityLabels() {
    if ( this.mapInstance.getLayer('city-labels') ) {
      (this.mapInstance.getSource('city-labels') as mapboxgl.GeoJSONSource)
        .setData( (this.cityLabelsCollection as GeoJSON.FeatureCollection) );
    } else {
      this.mapInstance.addLayer({
        id: 'city-labels',
        type: 'symbol',
        source: {
          type: 'geojson',
          data: (this.cityLabelsCollection as GeoJSON.FeatureCollection)
        },
        layout: {
          'text-field': '{name}',
          'text-font': ['Roboto Regular', 'Arial Unicode MS Bold'],
          'text-size': 10,
          'text-offset': [0, 2],
          'text-anchor': 'top'
        },
        paint: {
          'text-color': '#4d4d4d'
        }
      });
    }
  }

  private clearMarkers() {
    if (this.markers && this.markers.length > 0) {
      for (const m of this.markers) {
        m.remove();
      }
    }
  }

  private clearClusters() {
    if ( this.mapInstance.getLayer('clusters') ) { this.mapInstance.removeLayer('clusters'); }
    if ( this.mapInstance.getLayer('cluster-count') ) { this.mapInstance.removeLayer('cluster-count'); }
    if ( this.mapInstance.getLayer('unclustered-point') ) { this.mapInstance.removeLayer('unclustered-point'); }

    if ( this.mapInstance.getSource('clusterSource') ) { this.mapInstance.removeSource('clusterSource'); }
    this.clusterData.features = [];
  }

  private getMarkerParams(type: string) {
    const params: any = {};

    switch (type) {
      case 'militrkommandantur':
        params.radius = 8;
        params.index = 110;
        break;
      case 'capo-provincia':
        params.radius = 15;
        params.index = 100;
        break;
      case 'comando-militare-provinciale':
        params.radius = 20;
        params.index = 90;
        break;
      case 'comando-militare-regionale':
        params.radius = 25;
        params.index = 80;
        break;
      case 'corte-dappello':
        params.radius = 35;
        params.index = 70;
        break;
      case 'tribunale-militare-territoriale':
        params.radius = 45;
        params.index = 60;
        break;
      case 'tribunale-militare-straordinario-di-guerra':
        params.radius = 5;
        params.index = 120;
        params.position = 'top-right';
        break;
      case 'tribunale-speciale-per-la-difesa-dello-stato':
        params.radius = 5;
        params.index = 130;
        params.position = 'bottom-right';
        break;
      case 'sezione-tribunale-speciale-per-la-difesa-dello-stato':
        params.radius = 5;
        params.index = 130;
        params.position = 'bottom-right';
        break;
      case 'tribunale-speciale-straordinario':
        params.radius = 5;
        params.index = 140;
        params.position = 'bottom-left';
        break;
    }

    return params;
  }

  private assign(obj: any, prop: any, value: any) {
    if (typeof prop === 'string') {
      prop = prop.split('.');
    }
    if (prop.length > 1) {
      const e = prop.shift();
      this.assign(obj[e] =
        Object.prototype.toString.call(obj[e]) === '[object Object]'
          ? obj[e]
          : {},
        prop,
        value);
    } else {
      obj[prop[0]] = value;
    }
  }

  public computeCities(currentMap: any, currentPeriodID: any, data: any) {
    this.clearMarkers();
    this.clearClusters();

    let count = 0;
    this.cityLabelsCollection.features = [];

    for (const mapCityID in currentMap.cities) {
      if (mapCityID) {
        if (currentMap.cities[mapCityID].enabled && data.cities[mapCityID].location) {
          const city = data.cities[mapCityID];

          const periodData = city.periods[currentPeriodID];
          for (const periodDataCategoryID in periodData) {
            if (periodDataCategoryID && periodData[periodDataCategoryID].enabled) {
              this.addCityMarker(city, data.categories[periodDataCategoryID], count);
              const cityLabelFeature: GeoJSON.Feature = {
                type: 'Feature',
                properties: city,
                geometry: {
                  type: 'Point',
                  coordinates: [city.location.longitude, city.location.latitude]
                }
              };

              this.cityLabelsCollection.features.push( cityLabelFeature );
              count++;
            }
          }
        }
      }
    }

    this.addCityLabels();
  }

  public computeClusters(currentMap: any, currentPeriodID: any, data: any) {
    this.clearMarkers();
    this.clearClusters();

    let count = 0;
    this.cityLabelsCollection.features = [];

    for (const mapCityID in currentMap.cities) {
      if (mapCityID && currentMap.cities[mapCityID].enabled && data.cities[mapCityID].location) {
        const city = data.cities[mapCityID];
        const periodData = city.periods[currentPeriodID];
        for (const periodDataCategoryID in periodData) {
          if (
            periodDataCategoryID &&
            periodData[periodDataCategoryID].enabled &&
            periodData[periodDataCategoryID].quantity > 0
          ) {
            const clusterPoint: GeoJSON.Feature = {
              type: 'Feature',
              properties: {
                cityID: city.id,
                cityName: city.name,
                quantity: periodData[periodDataCategoryID].quantity,
                catID: data.categories[periodDataCategoryID].id,
                catName: data.categories[periodDataCategoryID].name,
                color: data.categories[periodDataCategoryID].color
              },
              geometry: {
                type: 'Point',
                coordinates: [city.location.longitude, city.location.latitude]
              }
            };
            this.clusterData.features.push(clusterPoint);
            count++;
          }
        }
      }
    }

    this.renderClusters();
    this.addCityLabels();
  }

  public computeMarkerGroups(currentMap: any, currentPeriodID: any, data: any) {
    this.clearMarkers();
    this.clearClusters();

    let count = 0;
    this.cityLabelsCollection.features = [];

    for (const mapCityID in currentMap.cities) {
      if (mapCityID && currentMap.cities[mapCityID].enabled && data.cities[mapCityID].location) {
        const city = data.cities[mapCityID];

        const periodData = city.periods[currentPeriodID];

        const markerGroupData = {
          groupCity: city,
          data: []
        };

        if (periodData) {
          for (const periodDataCategoryID in periodData) {
            if (
              periodDataCategoryID
              && periodData[periodDataCategoryID].enabled
              && periodData[periodDataCategoryID].quantity > 0
            ) {
              const cityLabelFeature: GeoJSON.Feature = {
                type: 'Feature',
                properties: city,
                geometry: {
                  type: 'Point',
                  coordinates: [city.location.longitude, city.location.latitude]
                }
              };

              this.cityLabelsCollection.features.push( cityLabelFeature );

              periodData[periodDataCategoryID].category = data.categories[periodDataCategoryID];
              markerGroupData.data.push( periodData[periodDataCategoryID] );
              count++;
            }
          }

          if (markerGroupData.data.length > 0) {
            this.addMarkerGroup( markerGroupData, count );
          }
        }
      }
    }

    this.addCityLabels();
  }

  public computeLayers(currentMap: any, currentPeriodID: any, data: any) {
    for (const layerID in data.layers) {
      if (layerID && currentMap.layers[layerID].enabled && data.layers[layerID].geometries[currentPeriodID]) {
        if (data.layers[layerID].properties[currentPeriodID].type === 'LINES') {
          this.firstLineLayerID = layerID;
          this.renderLineLayer(
            data.layers[layerID].id,
            data.layers[layerID].properties[currentPeriodID],
            data.layers[layerID].geometries[currentPeriodID]
          );
        }
        if (data.layers[layerID].properties[currentPeriodID].type === 'AREAS') {
          this.renderAreaLayer(
            data.layers[layerID].id,
            data.layers[layerID].properties[currentPeriodID],
            data.layers[layerID].geometries[currentPeriodID]
          );
        }
      } else {
        if (this.mapInstance.getLayer( layerID )) {
          this.mapInstance.removeLayer( layerID );
          this.mapInstance.removeSource( layerID );
        }
      }
    }
  }

  public toggleLayer(layerID: string) {
    const visibility = this.mapInstance.getLayoutProperty(layerID, 'visibility');
    if (visibility === 'visible') {
      this.mapInstance.setLayoutProperty(layerID, 'visibility', 'none');
    } else {
      this.mapInstance.setLayoutProperty(layerID, 'visibility', 'visible');
    }
  }

  public toggleCategory(categoryID: string) {
    for (const marker of this.markers) {
      const markerElement = marker.getElement();
      if (markerElement.classList.contains(categoryID)) {
        if ( this.categorySwitches[categoryID].visible ) {
          markerElement.classList.remove('hidden');
        } else {
          markerElement.classList.add('hidden');
        }
      }
    }
  }

  public showAllCategories() {
    for (const marker of this.markers) {
      marker.getElement().classList.remove('hidden');
    }
  }

  public hideAllCategories() {
    for (const marker of this.markers) {
      marker.getElement().classList.add('hidden');
    }
  }

  public zoomIn() {
    this.mapInstance.zoomIn();
  }

  public zoomDefault() {
    this.mapInstance.setZoom( this.defaultZoom );
  }

  public zoomOut() {
    this.mapInstance.zoomOut();
  }

  onResized(e: ResizedEvent) {
    if (this.mapInstance && e.newHeight > 0) {
      this.mapInstance.resize();
    }
  }
}
