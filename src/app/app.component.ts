import { Component, OnInit, OnDestroy, ViewChild, Sanitizer, SecurityContext } from '@angular/core';

import { FirebaseService } from './services/firebase.service';
import { CookieService } from 'ngx-cookie-service';
import { SymbolsService } from './services/symbols.service';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { MapComponent } from './map/map.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
  @ViewChild(MapComponent) map: MapComponent;

  title = 'Mappe della resistenza';
  showSpinner = true;

  maps: Observable<any>;
  layers: Observable<any>;
  cities: Observable<any>;
  categories: Observable<any>;
  periods: Observable<any>;

  dataObs: Observable<any>;
  dataSub: Subscription;
  data: any;

  currentMapID: string;
  currentMap: any;
  currentPeriodID: string;
  currentPeriod: any;
  currentCity: any;

  isLeftSidebarOpen = true;
  isRightSidebarOpen = false;
  canOpenRightSidebar = false;
  isIntroModalOpen = true;
  isMobilePeriodsMenuVisible = false;

  layerSwitches = {};
  categorySwitches = {};
  allCategoriesOn = true;
  showAllCategoriesSwitch = false;
  showAllCategoriesSwitchTimeOut = 1200;

  constructor(
    private firebase: FirebaseService,
    private symbolsService: SymbolsService,
    private sanitizer: Sanitizer,
    private cookies: CookieService
  ) {}

  ngOnInit() {
    if ( this.cookies.check('introSeen') ) { this.isIntroModalOpen = false; }

    this.maps = this.firebase.getCollection('/map-views');
    this.layers = this.firebase.getOrderedCollection('/layers', 'order', 1);
    this.cities = this.firebase.getCollection('/cities');
    this.categories = this.firebase.getOrderedCollection('/categories', 'order', 1);
    this.periods = this.firebase.getOrderedCollection('/periods', 'startDate', 1);

    this.dataObs = combineLatest( this.maps, this.layers, this.cities, this.categories, this.periods );
    this.dataSub = this.dataObs.subscribe(
      ([res1, res2, res3, res4, res5]) => {
        this.currentMapID = res1[0].id;
        this.currentMap = res1[0];
        this.currentPeriodID = res5[0].id;

        this.data = {
          maps: this.remapArrayToObject(res1),
          layers: this.remapArrayToObject(res2),
          cities: this.remapArrayToObject(res3),
          categories: this.remapArrayToObject(res4),
          periods: this.remapArrayToObject(res5)
        };

        this.initLayerSwitches();
        this.initCategorySwitches();

        this.renderMap();
        this.showSpinner = false;
        this.dataSub.unsubscribe();

        const to = setTimeout(() => { 
          this.showAllCategoriesSwitch = true;
          clearTimeout(to);
        }, this.showAllCategoriesSwitchTimeOut);
      }
    );
  }

  onMapLoaded() {
    console.log('map loaded');
  }

  remapArrayToObject(array: any) {
    const remappedObj = {};
    array.map(
      (item: any) => {
        remappedObj[item.id] = item;
      }
    );
    return remappedObj;
  }

  remapObjectToArray(obj: any) {
    const remappedArray = [];
    for (const item in obj) {
      if (item) {
        remappedArray.push(obj[item]);
      }
    }
    return remappedArray;
  }

  initLayerSwitches() {
    for (const layerID in this.data.layers) {
      if (layerID) {
        this.layerSwitches[layerID] = {visible: true};
      }
    }
  }

  initCategorySwitches() {
    for (const categoryID in this.data.categories) {
      if (categoryID) {
        this.categorySwitches[categoryID] = {visible: true};
      }
    }
  }

  ngOnDestroy() {
    if (this.dataSub) { this.dataSub.unsubscribe(); }
  }

  setCurrentPeriod(e: MouseEvent, period: any) {
    e.preventDefault();
    this.currentPeriodID = period.id;
    if (this.currentCity) { this.remapCurrentPeriodArray(); }
    this.renderMap();
  }

  onMarkerClick(cityID: string) {
    this.currentCity = this.data.cities[cityID];
    this.remapCurrentPeriodArray();
    this.canOpenRightSidebar = true;
    this.openSidebar('right');
  }

  onMapSelect(selectedMapID: string) {
    this.isRightSidebarOpen = false;
    this.canOpenRightSidebar = false;

    this.currentMapID = selectedMapID;
    this.currentMap = this.data.maps[this.currentMapID];

    this.allCategoriesOn = true;
    for (const catSwitch in this.categorySwitches) {
      if (catSwitch) {
        this.categorySwitches[catSwitch].visible = true;
      }
    }
    this.map.showAllCategories();

    this.renderMap();
  }

  onLayerClick(e: MouseEvent, layerID: string) {
    e.preventDefault();
    this.layerSwitches[layerID].visible = !this.layerSwitches[layerID].visible;
    this.map.toggleLayer( layerID );
  }

  onCategorySelect(e: MouseEvent, categoryID: string) {
    e.preventDefault();
    this.categorySwitches[categoryID].visible = !this.categorySwitches[categoryID].visible;
    if (this.categorySwitches[categoryID].visible) {
      this.allCategoriesOn = true;
    }

    switch (this.currentMap.type) {
      case 'HTMLMarkers':
        this.map.toggleCategory( categoryID );
        break;
      case 'markerGroups':
        this.renderMap();
        break;
    }
  }

  switchAllCategories(e: MouseEvent) {
    e.preventDefault();
    this.allCategoriesOn = !this.allCategoriesOn;

    for (const catSwitch in this.categorySwitches) {
      if (catSwitch) {
        this.categorySwitches[catSwitch].visible = this.allCategoriesOn;
      }
    }

    switch (this.currentMap.type) {
      case 'HTMLMarkers':
        // this.map.toggleAllCategories( categoryID );
        this.allCategoriesOn ? this.map.showAllCategories() : this.map.hideAllCategories();
        break;
      case 'markerGroups':
        // this.map.toggleMarkergroupSubItem( categoryID );
        this.renderMap();
        break;
    }
  }

  renderMap() {
    this.map.layerSwitches = this.layerSwitches;
    this.map.categorySwitches = this.categorySwitches;

    this.map.computeLayers( this.currentMap, this.currentPeriodID, this.data );

    switch (this.currentMap.type) {
      case 'HTMLMarkers':
        this.map.computeCities(this.currentMap, this.currentPeriodID, this.data);
        break;
      case 'clusterMarkers':
        this.map.computeClusters(this.currentMap, this.currentPeriodID, this.data);
        break;
      case 'markerGroups':
        this.map.computeMarkerGroups(this.currentMap, this.currentPeriodID, this.data);
        break;
    }
  }

  remapCurrentPeriodArray() {
    this.currentPeriod = [];

    let count = 0;

    switch (this.currentMap.type) {
      case 'HTMLMarkers':
        for (const catID in this.currentCity.periods[this.currentPeriodID]) {
          if (
            catID
            && this.currentCity.periods[this.currentPeriodID][catID].enabled
            && this.currentCity.periods[this.currentPeriodID][catID].quantity === 0
          ) {
            this.currentPeriod.push({
              data: this.data.categories[catID],
              // isSummaryOpen: count === 0 ? true : false,
              isSummaryOpen: false,
              isDescriptionOpen: false,
              ...this.currentCity.periods[this.currentPeriodID][catID]
            });
            count++;
          }
        }
        break;
      case 'clusterMarkers':
        for (const catID in this.currentCity.periods[this.currentPeriodID]) {
          if (
            catID
            && this.currentCity.periods[this.currentPeriodID][catID].enabled
            && this.currentCity.periods[this.currentPeriodID][catID].quantity > 0
          ) {
            this.currentPeriod.push({
              data: this.data.categories[catID],
              // isSummaryOpen: count === 0 ? true : false,
              isSummaryOpen: false,
              isDescriptionOpen: false,
              ...this.currentCity.periods[this.currentPeriodID][catID]
            });
            count++;
          }
        }
        break;
      case 'markerGroups':
        for (const catID in this.currentCity.periods[this.currentPeriodID]) {
          if (
            catID
            && this.currentCity.periods[this.currentPeriodID][catID].enabled
            && this.currentCity.periods[this.currentPeriodID][catID].quantity > 0
          ) {
            this.currentPeriod.push({
              data: this.data.categories[catID],
              // isSummaryOpen: count === 0 ? true : false,
              isSummaryOpen: false,
              isDescriptionOpen: false,
              ...this.currentCity.periods[this.currentPeriodID][catID]
            });
            count++;
          }
        }
        break;
    }

    this.currentPeriod.sort(
      (a: any, b: any) => {
        if (a.data.order > b.data.order) {
          return 1;
        }
        if (b.data.order > a.data.order) {
          return -1;
        }
        return 0;
      }
    );
  }

  toggleListItem(e: MouseEvent, cat: any) {
    e.preventDefault();
    cat.isSummaryOpen = !cat.isSummaryOpen;
    if (!cat.isSummaryOpen ) { cat.isDescriptionOpen = false; }
  }

  toggleListItemDescription(e: MouseEvent, cat: any) {
    e.preventDefault();
    cat.isDescriptionOpen = !cat.isDescriptionOpen;
  }

  toggleSidebar(e: MouseEvent, which: string) {
    e.preventDefault();
    switch (which) {
      case 'left':
        this.isLeftSidebarOpen = !this.isLeftSidebarOpen;
        break;
      case 'right':
        this.isRightSidebarOpen = !this.isRightSidebarOpen;
        break;
    }
  }

  openSidebar(which: string) {
    switch (which) {
      case 'left':
        this.isLeftSidebarOpen = true;
        break;
      case 'right':
        this.isRightSidebarOpen = true;
        break;
    }
  }

  closeSidebar(which: string) {
    switch (which) {
      case 'left':
        this.isLeftSidebarOpen = false;
        break;
      case 'right':
        this.isRightSidebarOpen = false;
        break;
    }
  }

  openIntroModal(e: MouseEvent) {
    e.preventDefault();
    this.isIntroModalOpen = true;
  }

  closeIntroModal() {
    this.isIntroModalOpen = false;
    this.cookies.set('introSeen', 'true');
  }

  toggleMobilePeriodsMenu() {
    this.isMobilePeriodsMenuVisible = !this.isMobilePeriodsMenuVisible;
  }
}
