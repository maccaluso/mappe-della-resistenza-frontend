import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  public listSize = 0;

  constructor(
    private db: AngularFirestore
  ) { }

  getList(path: string) {
    // console.log(path);
    return new Observable<any>();
  }

  getCollection(path: string) {
    // console.log('get collection');
    return this.db.collection<any>(path).snapshotChanges().pipe(
      take(1),
      map(items => items.map(item => {
        const data = item.payload.doc.data();
        const id = item.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  getOrderedCollection(path: string, orderField: string, orderDirection: number) {
    // console.log('get collection');
    return this.db.collection(path, ref => ref.orderBy( orderField, orderDirection === 1 ? 'asc' : 'desc' )).snapshotChanges().pipe(
      take(1),
      map(items => items.map(item => {
        const data = item.payload.doc.data();
        const id = item.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  createCollection(payload: any) {
    return this.db.collection<any>(payload);
  }

  getDocument(path: string) {
    return this.db.doc( path );
  }

  updateDocument(path: string, payload: any) {
    return this.db.doc( path ).update( payload );
  }

  createDocument(path: string, payload: any) {
    return this.db.collection( path ).add(payload);
  }

  deleteDocument(path: string, key: string) {
    return this.db.collection( path ).doc(key).delete();
  }

  buildGeopoint(lng: number, lat: number) {
    return new firebase.firestore.GeoPoint(lng, lat);
  }
}
